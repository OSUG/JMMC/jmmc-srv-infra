JMMC auth
=========

This module is an auth service for all JMMC systems
it is built in 3 parts

- a library, accessing various authentication repositories
- a web site, allowing the modification of the local repository
- a web service, managing sessions for the various web applications that use it

The library
-----------
