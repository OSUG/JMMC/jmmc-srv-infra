Systeme d'automatisation de la production du JMMC

# Work In Progress !!! 

- nécessaire :
  - ansible HEAD V2.2 min  avec les sous-modules:
      $ git clone https://github.com/ansible/ansible.git 
      $ git submodule update --init --recursive 

  - python >= 2.6
  - docker-py >= 1.7.0
  - Docker API >= 1.20

  - `ansible-galaxy install debops.ifupdown` (TBC)

Environnement testés: debian jessie
  Certains roles jmmc-docker necessitent une installe spécialisée



# Utilisation
Lancer les playbooks avec la commande run (utiliser tab pour la completion de la liste des roles)

Depuis la machine controleur:
- copier `logins.dist.yml` dans `logins.vars.yml`
- modifier `logins.vars.yml` pour mettre les bonnes valeurs
ajouter les deux lignes au .bashrc:
```
# enable jmmc-srv-infra commands
source $HOME/jmmc-srv-infra/bin/functions preprod
```



# Configuration

un environnement d'exécution nommé `test` est composé de :

-   un script `test` comportant les étapes a lancer
-   un fichier de variables `group_vars/test`
-   de fichiers d'inventaires aux noms suivants :<br/>
    ces fichiers d'inventaire doivent contenir une section `[test:children]` afin que les variables du fichier `group_vars/test` soient chargées
    -   `jmmc-test.inventory`<br/>
        contenant les informations nécessaires pour la construction de la machine controlleur
    -   `jmmc-test.control.inventory`<br/>
        contenant les informations sur toutes les machines qui devront etre manipulées par le controlleur

## Contenu du fichier de variables

### Spécification de la configuration de l'hyperviseur

#### Définitions liées a l'usage d'un hyperviseur

```
vm_mgr:
  type: ['none'|'kvm']
```
peut être:

-   "none" dans le cas ou on ne gere pas le systeme de gestion de machines virtuelles
-   "kvm" si on utilise le systeme KVM/QEMU

```
pxe_nginx: '<valeur>'
```
répertoire dans lequel sont stockés les données a offrir au téléchargement pour le systeme d'installation par réseau (limité a `vm_mgr: 'kvm'` pour l'instant)

#### Définitions liées au réseau

```
no_private_net: ['True'|'False']
```
utilise t'on un réseau privé entre le controlleur et les workers<br/>
Uniquement utilisé quand le `vm_mgr` est `kvm`

```
ns_zone: '<valeur>'
```
nom de la zone dns a l'intérieur du réseau privé sus-cité

```
public_br:
  type: 'public'
  name: 'vms0'
  ip_addr:            '10.11.0.1/24'
  dhcp:
    first_ip:           '10.11.0.10'
    last_ip:            '11.11.0.50'
```
definition du réseau public. on definit son type, son nom (le nom sous lequel il apparaitra dans les informations réseau de l'hote), l'adresse ip du routeur de sortie (typiquement l'hyperviseur dans le cas de kvm) ainsi que le bloc d'adresses disponibles pour dhcp

```
priv_br:
  type: 'private'
  name: 'br-int-01'
```
définition du réseau privé, uniquement utilisé quand `no_private_net` est `False`

#### Définitions liées aux machines

une section `machines` est définie ainsi :

```
machines:
  <définition de machine 1>
  <définition de machine 2>
  (...)
```
Chaque machine est définie par un paragraphe pouvant contenir les informations suivantes :

```
  '<nom de machine>':
    template:    '<nom de fichier>'
    public_mac:  'aa:bb:cc:dd:ee:ff'
    public_ip:   'g.h.i.j/k'
    priv_mac:    'll:mm:nn:oo:pp:qq'
    priv_ip:     'r.s.t.u/v'
    tftp_dir:    '<chemin de répertoire>'
```

Détail du contenu des variables :

```
template: '<nom de fichier>'
```
Nom d'un fichier dans le répertoire `/templates/` contenant une définition des caractéristiques de la vm 

```
public_mac:  'aa:bb:cc:dd:ee:ff'
```

Adresse mac a affecter a l'interface sur le réseau public (défini dans la section précédente)

```
public_ip:   'g.h.i.j/k'
```

Adresse IPv4 sur cette interface sur le réseau public

```
priv_mac:    'll:mm:nn:oo:pp:qq'
```

Adresse mac a affecter a l'interface sur le réseau privé

```
priv_ip:     'r.s.t.u/v'
```

Adresse IPv4 sur cette interface sur le réseau privé

```
tftp_dir:    '<chemin de répertoire>'
```

Répertoire de la machine virtuelle dans lequel installer les fichier nécessaires pour le serveur tftpd pour le pxe boot<br/>
Les fichiers nécessaires se trouvent dans le répertoire `/roles/ctrl-pxe-tftpd-srv/files/srv/tftpboot`, ainsi que la template `/roles/jmmc-vm/templates/pxelinux.cfg.j2` qui est instantiée pour chaque machine

#### Définitions liées au services

Tous les services vers lesquels le front-end redirige doivent etre définis dans cette section:

```
services:
  vars:
    templates_dir: 'services'
  <définition service 1>
  <définition service 2>
  (...)
```
la variable `templates_dir` définit le répertoire ou sont stockés les fragments de configuration dans le repertoire `templates`

une arborescence exemple est :

```
./templates/services
./templates/services/jmmc-apps-test
./templates/services/jmmc-apps-test/haproxy.be.j2
./templates/services/jmmc-apps-test/haproxy.fe.j2
./templates/services/jmmc-www
./templates/services/jmmc-www/haproxy.be.j2
./templates/services/jmmc-www/haproxy.fe.j2
```

Chaque service est défini par les variables suivantes :

```
<nom du service>:
  container_type: '[docker]'
  host_machine:   '<nom de machine>'
  priority:       '[0-9]'
  fe_template:    '<template pour le front end>'
  be_template:    '<template pour le back end'
```

Détail des variables :

```
container_type: '[docker]'
```
Type de container dans lequel est installé le service 

```
host_machine:   '<nom de machine>'
```
Machine sur laquelle le service est hébergé

```
priority:       '[0-9]'
```
Priorité de ce fragment de configuration. Ordre dans lequel le fragment doit apparaitre dans la configuration du ha-proxy

```
fe_template:    '<template pour le front end>'
```
Template pour le fragment concernant le module front-end de ha-proxy

```
be_template:    '<template pour le back end'
```
Template pour le fragment concernant le module back-end de ha-proxy

```
serverprefix: '<valeur>'
```
prefix rajouté dans les sections de templates haproxy pour distinguer le cas de preprod et prod.
#### Définitions liées a la compilation d'applications

```
cert_dir: '<valeur>'
```
répertoire dans lequel sont stockés les divers certificat

```
codebase: '<valeur>'
```
codebase utilisée pour modifier le contenu des jnlp

```
applications:
  OIFitsExplorer:
    prod: ''
    beta: ''
    alpha: ''
  <autre application>
```

Liste des applications a publier avec leurs versions pour chaque niveau de release.

Pour l'instant, ne specifier qu une seule application et une seule release a la fois, admManager doit etre patché pour autoriser le build en parallele
