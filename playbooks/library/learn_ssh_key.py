#!/usr/bin/python
# -*- coding: utf-8 -*-

# Author: Raphaël Jacquot
# Description: learns the other machine's host ssh key (=> known_hosts)

from __future__ import print_function
from ansible.module_utils.basic import *
import sys, os, errno, os.path, subprocess, socket
import re, dns.inet, dns.resolver

class LearnSshKey (object):
    
    def remove_key (self, target):
        null = open('/dev/null', 'w')
        if type(target) is not list :
            target = [ target ]
        for t in target:
            try :
                out = subprocess.check_call (['ssh-keygen', '-R', t], stdin=None, stdout=null, stderr=subprocess.STDOUT, shell=False)
            except subprocess.CalledProcessError as e:
                try:
                    r = e.returncode
                except AttributeError as e:
                    r = None
                try:
                    c = e.cmd
                except AttributeError as e:
                    c = None
                try:
                    o = e.out
                except AttributeError as e:
                    o = None
                if r == 255:
                    # there was nothing to remove... skip
                    pass
                else:
                    response = unicode(e)
                    self.module.fail_json (msg = response+u'\nssh-keygen returned '+unicode(r)+u'\n'+unicode(c)+u'\n'+unicode(o))
        null.close()

    def add_key (self, target):
        null = open(os.path.expanduser('~/ssh-keyscan.log'), 'a')
        # open file
        kh_name = os.path.expanduser('~/.ssh/known_hosts')
        kh_file = open(kh_name, 'a')
        if type(target) is not list :
            target = [ target ]
        for t in target:
            try :
                out = subprocess.check_output (['ssh-keyscan', t], stdin=None, stderr=null, shell=False)
                null.write ('GOT \"'+str(out)+'\" FROM STDOUT')
                null.flush()
            except subprocess.CalledProcessError as e:
                self.module.fail_json (msg = self.message+u'\n'+u'ssh-keyscan returned '+unicode(e.returncode)+u'\n'+unicode(e.cmd))
            kh_file.write (out)
            kh_file.flush ()
        kh_file.close()
        null.close()

    def update_machine (self, address, name):
        if address is not None : 
            self.remove_key (address)
            self.add_key(address)
        if name is not None:
            self.remove_key (name)
            self.add_key(name)
    
    def do (self):
        # identify targets
        try:
            af = dns.inet.af_for_address(self.machine)
        except ValueError as e:
            # we have a machine name try to find the ip address
            error = u''
            try:
                a = dns.resolver.query (self.machine, 'A')
            except dns.resolver.NoNameservers as e:
                addr = []
            except dns.exception.Timeout as e :
                error = u'timeout trying to resolve A for \''+unicode(self.machine)+u'\''
                addr = []
            except dns.resolver.NXDOMAIN as e:
                addr = []
                try:
                    gai = socket.getaddrinfo(self.machine, 22)
                except socket.gaierror as e:
                    gai = None
                else:
                    for g in gai : 
                        a,p = g[4]
                        if a not in addr : 
                            addr.append(a)
                if len(addr) == 0 :
                    error = u'unable to resolve A for \''+unicode(self.machine)+u'\' NXDOMAIN - gai gives : '+unicode(gai)
            else :
                addr = []
                for rdata in a:
                    addr.append (rdata.address)
            message = 'updating 1 '+str(addr)+' '+str(self.machine)+' - '+error
            self.message+=message
            if len(addr) > 0:
                self.update_machine (addr, self.machine)
            else:
                self.update_machine (None, self.machine)
            self.module.exit_json (changed=True, msg=message)
        else:
            # ipv4 or ipv6
            revname = dns.reversename.from_address(self.machine)
            try:
                a = dns.resolver.query(revname, 'PTR')
            except dns.resolver.NXDOMAIN as e:
                # can't find a name for this machine
                self.update_machine (self.machine, None)
            else:
                self.module.fail_json(msg = "reversed name : "+str(a))
            self.module.exit_json (changed=True)

    def __init__ (self):
        self.message = ""
        self.module = AnsibleModule (
            argument_spec = {
                'state' : { 'default': 'present', 'options' : ['present', 'absent'] },
                'machine': { 'type': 'str' }
            }
        )

        self.state = self.module.params['state']
        self.machine = self.module.params['machine']

        self.present = (self.state == 'present')

        self.do ()

if __name__ == "__main__":
    LearnSshKey()
