#!/usr/bin/env python

import urlparse
import httplib
import json
import sys
import re

class Test (object):
    def __init__ (self, testfile):
        self.testfile = testfile
        self.load_testfile()

    def load_testfile (self):
        f = open(self.testfile)
        d = f.read()
        f.close()
        # we support // comments
#        d = re.sub(r'\\\n', '', d)
#        d = re.sub(r'//.*\n', '\n', d)
        self.config = json.loads(d)
        if "server" in self.config.keys():
            self.server = self.config["server"]
        else:
            print ("Configuration ERROR: missing <server> information in configuration data")
            sys.exit(1)
        if "tests" in self.config.keys():
            self.tests = self.config["tests"]
        else:
            print ("Configuration ERROR: missing <tests> information in configuration data")
            sys.exit(1)

    def doRequest (self, method, url):
        # analyse url, find if we have a server and port definition
        scheme, fqdn, path, query, fragment = urlparse.urlsplit(url)
        port=""
        # extract port from fqdn if present
        m = re.match("^(.*):(\d+)$", fqdn)
        if m:
            fqdn, port = m.groups()

        print ( scheme, fqdn, port, path, query, fragment)
:w
        if len(scheme) == 0 and "scheme" in self.server.keys():
            scheme = self.server["scheme"]
        else:
            scheme = "http"
        if len(fqdn) == 0: 
            if "fqdn" in self.server.keys():
                fqdn = self.server["fqdn"]
            else:
                print ("ERROR: missing server <fqdn> specification in configuration")
                sys.exit(1)
        
        if len(port)==0:
            if "port" in self.server.keys():
                port = self.server["port"]
            else:
                port = 80
        if len(path) == 0:
            path = "/"
        url = path
        if len(query) > 0:
            url+=u"?"+query
        if len(fragment) > 0:
            url+=u"#"+fragment
        if scheme == "http":
            conn = httplib.HTTPConnection (unicode(fqdn)+u":"+unicode(port))
        else:
            print("ERROR: unsupported scheme "+unicode(scheme))
            sys.exit(1)
        conn.connect ()
        conn.request (method, url)
        resp = conn.getresponse ()
        data = resp.read ()
        hdrs = resp.getheaders ()

        return (resp, hdrs, data)

    def handleRedirect (self, conf, index, resp, hdrs, data):
        print ("Got a redirection in return")
        block = conf[index]
        # check if the given location corresponds to the one configured
        if "location" in block.keys():
            loc = block["location"]
            l = [item[1] for item in hdrs if item[0] == 'location']
            print (loc, l)
            if len(l)==1:
                if loc == l[0]:
                    l = l[0]
                    # execute the required step for a redirect
                    print ("executing requested redirection to "+unicode(l))
                    url = l
                    if len(conf)==index+1:
                        resp, hdrs, data = self.doRequest(method, url)
                        print ("WARNING: missing configuration sub-blocks, server response after redirect was:")
                        print (resp.status)
                        print (hdrs)
                    return url 
                else:
                    print ("expected loc differs from server given value")
                    print (loc, l[0])
                    return False
            else:
                print ("ERROR: multiple locations given by server: "+unicode(l))
                return False
        else:
            print ("Configuration ERROR: expected <location> item in configuration block")
            return False
    
    def run (self):
        self.failed = []
        for t in self.tests:
            i = 0
            while i<len(t):
                c = t[i]
                #print (c)
                # we are at the first item of a block, prime the functional variables with required basic things
                # when handling the subsequent items of a block, filling those is handled by the response handler
                if i==0:
                    if "description" in c.keys():
                        description = c["description"]
                    else:
                        print ("Configuration ERROR: expected <description> configuration block")
                        self.failed.append("missing description ???")
                        break
                    if "method" in c.keys():
                        method = c["method"]
                    else:
                        print ("Configuration ERROR: expected <method> item in configuration block")
                        self.failed.append(description)
                        break
                    if "url" in c.keys():
                        url = c["url"]
                    else:
                        print ("Configuration ERROR: expected <url> item in configuration block")
                        self.failed.append(description)
                        break

                if i==0:
                  print ("\n---\n--- %s\n---\n"%(description,))
                resp, hdrs, data = self.doRequest(method, url)

                #print (hdrs)
                if "response" in c:
                    if resp.status == c["response"]:
                        if resp.status == 401:
                            print ("The request was valid, but the server is refusing action as expected - you may try to authenticate...")
                            # TODO we may go further providing auth in the test set to check that content behind is ok
                        elif resp.status == 403:
                            print ("The request was valid, but the server is refusing action as expected")
                        elif resp.status == 200:
                            print ("Got a document in return")
                            # check content-type if given. content-type is an re
                            if "content-type" in c.keys():
                                content_type = c["content-type"]
                                if type(content_type) is dict:
                                    if "type" in content_type.keys():
                                        if content_type["type"] == "re" and "re" in content_type.keys():
                                            exp = re.compile (content_type["re"])
                                        else:
                                            print ("Configuration ERROR: { \"type\": \"re\", \"re\": \"<regexp>\" } expected")
                                            self.failed.append(description)
                                            break
                                elif type(content_type) is str:
                                    exp = content_type
                                else:
                                    print("Configuration ERROR: expected <content_type> to be either a string or a dict")
                                    self.failed.append(description)
                                    break
                                ct = [item[1] for item in hdrs if item[0] == 'content-type']
                                if len(ct)==1:
                                    ct = ct[0]
                                    if type(exp) == type(re.compile('')):
                                        m = exp.match(ct)
                                        if m:
                                            print ("content-type is the one expected")
                                        else:
                                            print ("ERROR: content-type does not match regular expression : "+unicode(ct[0]))
                                            self.failed.append(description)
                                            break
                                    else:
                                        print ("ERROR: <expected_item> as string is not handled yet")
                                        self.failed.append(description)
                                        break
                                else:
                                    print ("ERROR: multiple content-type records given by server: "+unicode(ct))
                                    self.failed.append(description)
                                    break
                            else:
                                print (resp.status)
                                print (hdrs)
                        elif resp.status == 301 or resp.status == 302:
                            url = self.handleRedirect(t, i, resp, hdrs, data)
                            if type(url) is bool and url is False:
                                self.failed.append(description)
                                break
                        else:
                            print ("ERROR: unimplemented handling of response code "+unicode(resp.status))
                            self.failed.append(description)
                            break
                        # increment the counter
                        i+=1
                    else:
                        # we expected some other code
                        print ("ERROR: expected response code "+unicode(c["response"])+", the server returned "+unicode(resp.status)+" instead")
                        print (resp.status)
                        print (hdrs)
                        self.failed.append(description)
                        # exit the test loop, go to next block
                        break
                else:
                    print ("Configuration ERROR: expected <response> item in configuration block")
                    print (resp.status)
                    print (hdrs)
                    self.failed.append(description)
                    break
            print ("Finished handling configuration block")



if __name__ == "__main__":
    # open and read configuration file
    t = Test("new-infra.test")
    # start testing stuff
    t.run ()
    print ("\n\nTesting on %s://%s:%d"%(t.server["scheme"], t.server["fqdn"], t.server["port"]) )
    print ( "%d failure(s) over %d tests" % (len(t.failed), len(t.tests)))
    for e in t.failed:
        print ("  - ERROR - %s"%(e))
    sys.exit(len(t.failed))
