#!/usr/bin/env python

#LOG_FILE = 'yourlogfile.log'
HOST, PORT = "0.0.0.0", 515
ELASTIC    = "jmmc-ctrl-1.ujf-grenoble.fr:9200"


import logging
import SocketServer
import re
import datetime
#import http.client
import httplib2
import json

#logging.basicConfig(level=logging.INFO, format='%(message)s', datefmt='', filename=LOG_FILE, filemode='a')

class SyslogUDPHandler(SocketServer.BaseRequestHandler):
    def parse(self, data): 
        # find the first ']'
        #p = re.match('haproxy\[(?<pid>\d+)\]:\ (?<client_ip>\d+\.\d+\.\d+\.\d+):(?<server_port>\d+)\ (?<rest>.*)$', data)
        #p = re.match('^(<\d+>.{16})(.*)$', data)
        dt = datetime.datetime.now()
        p = re.match(
            '^(?P<pre><\d+>.{16})'+
            'haproxy\[(?P<server_pid>\d+)\]:\ '
            '(?P<client_ip>\d+\.\d+\.\d+\.\d+):(?P<server_port>\d+)\ '+
            '\[(?P<date>[^\]]*)\]\ '+
            '(?P<frontend>\S+)\ '+
            '(?P<backend>\S+)\ '+
            '(?P<timings>\S+)\ '+
            '(?P<http_status_code>\d+)\ '+
            '(?P<bytes_read>\d+)\ '+
            '(?P<request_cookie>\S+)\ '+
            '(?P<response_cookie>\S+)\ '+
            '(?P<termination_state>\S+)\ '+
            '(?P<haproxy_stats>\S+)\ '+
            '(?P<haproxy_queues>\S+)\ '+
            '(?P<http_request>.*)$', data)
        # generate index value
        #idx = '/haproxy/'+dt.isoformat()
        method = 'POST'
        idx= '/haproxy/'
        data = json.dumps(p.groupdict())
        print (idx+' - '+data)
        
        #h = http.client.HTTPConnection(ELASTIC)
        #h.request('PUT', idx, body=json.dumps(p))
        #r = h.getresponse()
        #response = r.read()
        #print (response)

        http = httplib2.Http()
        url = 'http://'+ELASTIC+idx
        response, content = http.request(url, method, body=data)
        print (response, content)

    def handle(self):
        data = bytes.decode(self.request[0].strip())
        socket = self.request[1]
        #print ("%s : "%self.client_address[0], str(data))
        self.parse (str(data))

if __name__ == "__main__":
    try:
        server = SocketServer.UDPServer ((HOST,PORT), SyslogUDPHandler)
        server.serve_forever(poll_interval=0.5)
    except (IOError, SystemExit):
        raise
    except KeyboardInterrupt:
        print ("CTRL+C - shutting down")
