#!/usr/bin/python
# -*- coding: utf-8 -*-

# Author: Raphaël Jacquot
# Description: corrects the contents of the jnlp file for one application


from __future__ import print_function
from ansible.module_utils.basic import *
import sys, os, errno, os.path, re, subprocess
import xml.etree.ElementTree as et

class FixFiles (object): 

    def replace_in (self, e, attr):
        a = e.get(attr)
        l = len(self.replace_url)
        if a[0:l] == self.replace_url :
            na = a[l:]
            e.set(attr, self.base_url+na)
            return True
        return False

    def replace_in_content (self, e):
        # only in the first part of the xml element's text
        t = e.text
        p = t.find(self.replace_url)
        if p == -1:
            return False
        t1 = t[:p]
        l = len(self.replace_url)
        t2 = t[(p+l):]
        newval = t1+self.base_url+t2
        self.errors.append("'%s' %d ( '%s' - '%s' ) => '%s'"%(t,p,t1,t2,newval))
        e.text = newval
        return True

    # hack one file file at a time
    # only handle xml based files
    def fix_file (self, file_name) :
        self.errors.append("%s mode : %s"%(self.mode,file_name))
        #if self.mode == "html":
        #    FNULL = open(os.devnull, 'w')
        #    ret = subprocess.call (['tidy', '-w', '132', '-imn', '--output-xml', '1', file_name], stdout=FNULL, stderr=subprocess.STDOUT)
        #    FNULL.close()
        #    self.errors.append("tidy returns %d"%(ret))
        changed = False
        try:
            tree = et.parse (file_name)
        except et.ParseError as e:
            self.errors.append ('parse error in '+file_name+'\n'+unicode(e))
            return False
        root = tree.getroot()

        for e in root.iter():
            if self.mode == 'jnlp':
                if e.tag=='jnlp':
                    if self.replace_in(e, 'codebase'):
                        changed = True
                if e.tag=='icon':
                    if self.replace_in(e, 'href'):
                        changed = True
                if e.tag=='jar':
                    if self.replace_in(e, 'href'):
                        changed = True

        # this is for the htm file
            if self.mode == 'html':
                if e.tag=='img':
                    if self.replace_in(e, 'src'):
                        changed = True
                if e.tag=='pre':
                    if self.replace_in_content(e):
                        changed = True
        if changed:
            tree.write (file_name)
            self.modified.append (file_name)

    # list all jnlp files in the "path" directory
    def do (self) :
        self.flist = []
        # list all files in path directory
        s = ''
        jnlp_ext = ['.jnlp']
        html_ext = ['.html','.htm']
        for root, dirs, files in os.walk (self.path, followlinks = True):
            for name in files:
                filename, ext = os.path.splitext(name)
                path = os.path.join(root,name)
                if self.mode == 'jnlp':
                    if ext in jnlp_ext:
                        self.flist.append(path)
                elif self.mode == 'html':
                    if ext in html_ext:
                        self.flist.append(path)
        for j in self.flist:
            self.fix_file (j)

    def __init__ (self):
        self.errors = []
        self.modified = []
        self.allfiles = []
        self.module = AnsibleModule (
            argument_spec = {
                'mode':        { 'type': 'str' },
                'path':        { 'type': 'str' },
                'replace_url': { 'type': 'str' },
                'base_url':    { 'type': 'str' }
            }
        )

        self.mode =        self.module.params['mode']
        self.path =        self.module.params['path']
        self.replace_url = self.module.params['replace_url']
        self.base_url =    self.module.params['base_url']

        self.do ()

        changed = len(self.modified) > 0 
        self.module.exit_json(changed=changed, modified_files=self.modified, looked_at=self.flist, all_files=self.allfiles, errors=self.errors)


if __name__ == "__main__":
    FixFiles()

