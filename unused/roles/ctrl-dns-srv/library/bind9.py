#!/usr/bin/python
# -*- coding: utf-8 -*-

from __future__ import print_function
from ansible.module_utils.basic import *
import sys, os, errno, os.path
import re

BIND_CONFIG_DIR="/etc/bind"

# zone list stuff
ZONE_LIST_FILENAME          = u"named.conf.local"

ZONE_LIST_COMMENT           = u"//"
ZONE_LIST_ANSIBLE           = u"ANSIBLE"
ZONE_LIST_ANSIBLE_MANAGED   = ZONE_LIST_COMMENT+u" "+ZONE_LIST_ANSIBLE+u" Managed"

ZONE_LIST_ZONE              = u"zone"

ZONE_LIST_TYPE                  = u"type"
ZONE_LIST_TYPE_DELEGATION_ONLY  = u"delegation-only"
ZONE_LIST_TYPE_FORWARD          = u"forward"
ZONE_LIST_TYPE_HINT             = u"hint"
ZONE_LIST_TYPE_IN_VIEW          = u"in-view"
ZONE_LIST_TYPE_MASTER           = u"master"
ZONE_LIST_TYPE_REDIRECT         = u"redirect"
ZONE_LIST_TYPE_SLAVE            = u"slave"
ZONE_LIST_TYPE_STATIC_STUB      = u"static-stub"
ZONE_LIST_TYPE_STUB             = u"stub"

ZONE_LIST_TYPE_LIST         = [ 
    ZONE_LIST_TYPE_DELEGATION_ONLY,
    ZONE_LIST_TYPE_FORWARD,
    ZONE_LIST_TYPE_HINT,
    ZONE_LIST_TYPE_IN_VIEW,
    ZONE_LIST_TYPE_MASTER,
    ZONE_LIST_TYPE_REDIRECT,
    ZONE_LIST_TYPE_SLAVE,
    ZONE_LIST_TYPE_STATIC_STUB,
    ZONE_LIST_TYPE_STUB]

ZONE_LIST_FILE              = u"file"

ZONE_LIST_OPEN_BRACE        = u"{"
ZONE_LIST_CLOSE_BRACE       = u"}"
ZONE_LIST_SEMICOLON         = u";"

ZONE_LIST_ERROR_PARSE_ERROR = 1

# zone file stuff
ZONE_COMMENT                = u";"
ZONE_ANSIBLE                = u"ANSIBLE"
ZONE_ANSIBLE_MANAGED        = ZONE_COMMENT+u" "+ZONE_ANSIBLE+" Managed"

ZONE_TTL                    = u"$TTL"
ZONE_ORIGIN                 = u"$ORIGIN"
ZONE_IN                     = u"IN"

ZONE_SOA                    = u"SOA"
ZONE_A                      = u"A"
ZONE_NS                     = u"NS"

class ZoneList(object):
    def __init__(self):
        self.tokens = []
        self.zones = {}
        self.error = None
        self.error_msg = None
        try:
            f = open(os.path.join(BIND_CONFIG_DIR, ZONE_LIST_FILENAME), "r")
        except IOError as e:
            if e.errno == errno.ENOENT:
                # can't find the file, skip reading it
                pass
        else :
            data = f.read()
            f.close()
            self.extract_tokens(data)
            if not self.parse():
                self.error = ZONE_LIST_ERROR_PARSE_ERROR
        
    def extract_token (self, c):
        if self.start_comment:
            if c == u"/":
                self.start_comment = False
                self.in_comment = True
                self.token += c
                return
            else:
                self.token += c
                return
        if self.in_comment:
            if c == u"\n":
                self.in_comment = False
                self.tokens.append(self.token)
                self.token = u""
                return
            self.token += c
            return
        if self.in_string:
            if c == u"\"":
                self.in_string = False
                self.token += c
                self.tokens.append(self.token)
                self.token = u""
            else:
                self.token += c
            return
        if c == u"\n":
            if len(self.token) > 0:
                self.tokens.append(self.token)
                self.token = u""
            return
        if c == u" ":
            if len(self.token) > 0:
                self.tokens.append(self.token)
                self.token = u""
            return
        if c == u"/":
            if len(self.token) > 0:
                self.tokens.append(self.token)
                self.token = u""
            self.start_comment = True
            self.token += c
            return
        if c == u"\"":
            if len(self.token) > 0:
                self.tokens.append(self.token)
                self.token = u""
            self.in_string = True
            self.token += c
            return
        if c in "{};":
            if len(self.token) > 0:
                self.tokens.append(self.token)
                self.token = u""
            self.tokens.append(c)
            return  
        self.token += c

    def extract_tokens (self, data):
        self.start_comment = False
        self.in_comment = False
        self.in_string = False
        self.token = u""
        i = 0
        while i<len(data):
            c = data[i]
            self.extract_token(c)
            i+=1
        if len(self.token) > 0:
            self.tokens.append(self.token)
            self.token = u""
        
    def get_token (self):
        while True:
            if len(self.tokens) == 0:
                return None
            t = self.tokens[0]
            self.tokens = self.tokens[1:]
            # skip comments
            if (len(t)==1) or ((len(t) >= 2) and (t[0:2] != u"//")):
                return t

    def is_string (self, token):
        if len(token)<2:
            return False
        if token[0] != "\"":
            return False
        if token[-1] != "\"":
            return False
        return True

    def string_remove_quotes (self, token):
        return token[1:-1]

    def parse_zone_def (self):
        zone = {}
        zone_name = self.get_token()
        if not self.is_string(zone_name):
            self.error_msg = "ERROR in zone_name: expected string, got '"+zone_name+"'"
            return False
        zone_name = self.string_remove_quotes(zone_name)
        ob = self.get_token()
        if ob != ZONE_LIST_OPEN_BRACE:
            self.error_msg = "ERROR in zone_def: expected '{', got '"+ob+"'"
            return False
        while True:
            t = self.get_token()
            if t == ZONE_LIST_CLOSE_BRACE:
                break
            if t == ZONE_LIST_TYPE:
                # read zone type then semicolon
                zone_type = self.get_token()
                if zone_type not in ZONE_LIST_TYPE_LIST:
                    self.error_msg = "ERROR in TYPE: zone_type not in list '"+zone_type+"'"
                    return False
                semicolon = self.get_token()
                if semicolon != ZONE_LIST_SEMICOLON:
                    self.error_msg = "ERROR in TYPE: expected ';', got '"+semicolon+"'"
                    return False
                zone[t] = zone_type
                continue
            if t == ZONE_LIST_FILE:
                filename = self.get_token()
                if not self.is_string(filename):
                    self.error_msg = "ERROR in FILE: got '"+filename+"'"
                    return False
                semicolon = self.get_token()
                if semicolon != ZONE_LIST_SEMICOLON:
                    self.error_msg = "ERROR in FILE: expected ';', got '"+semicolon+"'"
                    return False
                zone[t] = self.string_remove_quotes(filename)
                continue
        self.zones[zone_name] = zone
        return True

    def parse (self):
        while True: 
            t = self.get_token()
            if t is None:
                # we're at the EOF, everything went fine
                return True 
            if t == ZONE_LIST_ZONE:
                if not self.parse_zone_def():
                    return False

    def write(self):
        f = open(os.path.join(BIND_CONFIG_DIR, ZONE_LIST_FILENAME), "w")
        print (unicode(self), file=f)
        f.close()
        return True
   
    def add_quotes (self, s):
        return u"\""+s+u"\""

    def generate_zone_info_line (self, k):
        l = []
        l.append (ZONE_LIST_ZONE)
        l.append (self.add_quotes(k))
        l.append (ZONE_LIST_OPEN_BRACE)
        zi = self.zones[k]
        for ik in zi.keys():
            i = []
            i.append(ik)
            val = zi[ik]
            if ik in [ZONE_LIST_FILE]:
                val = self.add_quotes(val)
            i.append(val)
            i.append(ZONE_LIST_SEMICOLON)
            l.append (u" ".join(i))
        l.append (ZONE_LIST_CLOSE_BRACE)
        l.append (ZONE_LIST_SEMICOLON)
        return u" ".join(l)

    def __unicode__ (self):
        if self.error is not None:
            return unicode(self.error)+u" | "+unicode(self.error_msg)
        # generate zone file as string
        s = ZONE_LIST_ANSIBLE_MANAGED+u"\n"
        for zk in self.zones.keys():
            s += self.generate_zone_info_line (zk) + u"\n"
        return s

    def add_zone (self, zone_name, zone_filename):
        if zone_name in self.zones.keys():
            return False
        z = {}
        z[ZONE_LIST_TYPE] = ZONE_LIST_TYPE_MASTER
        z[ZONE_LIST_FILE] = zone_filename
        self.zones[zone_name] = z
        return True

    def remove_zone (self, zone_name):
        if zone_name not in self.zones.keys():
            return False
        del self.zones[zone_name]
        return True

#------------------------------------------------------------------------------
#
# zone file object
#
#

class Zone(object):
    def __init__(self):
        self.module    = None
        self.zone_list = {}
        self.present   = True
        self.changed   = False
        self.ttl       = 1800
        self.origin    = u"example.com."
        self.ns        = u"ns."+self.origin
        self.email     = None
        self.serial    = 1
        self.refresh   = self.ttl
        self.retry     = self.ttl
        self.expiry    = self.ttl
        self.minimum   = self.ttl
        self.records   = {}
 
    def set_new_origin (self, origin):
        c = origin[-1]
        if c!=u".":
            origin+=u"."
        self.origin = origin
        self.ns     = u"ns."+origin

    def init (self):
        self.zone_list = ZoneList()
        self.zone_list.module = self.module
        if self.present:
            # check if we have a zone
            self.read_zone()
        else:
            self.remove_zone()

    def filename(self):
        o = self.origin
        if o[-1]==u".":
            o=o[0:-1]
        return o+'.zone'

    #--------------------------------------------------------------------------
    # zone list managing functions
    #

    def add_zone (self):
        return self.zone_list.add_zone (self.origin, os.path.join(BIND_CONFIG_DIR, self.filename()))

    def set_present (self):
        self.present = True
        added = self.add_zone ()
        if added:
            self.changed = True

    def remove_zone (self):
        return self.zone_list.remove_zone (self.origin)

    def set_absent (self):
        self.present = False
        removed = self.remove_zone ()
        if removed:
            self.changed = True

    #--------------------------------------------------------------------------
    # zone record managing functions
    #

    def record_exists (self, name, rectype, value):
        if rectype not in self.records.keys():
            return False
        records = self.records[rectype]
        if name not in records.keys():
            return False
        values = records[name]
        if value not in values:
            return False
        return True
    
    def add_record (self, name, rectype, value):
        # alias name if necessary
        if name == self.origin:
            name = u"@"
        added = False
        if rectype not in self.records.keys():
            records = {}
        else:
            records = self.records[rectype]
        if name not in records.keys():
            values = []
        else:
            values = records[name]
        if value not in values:
            values.append(value)
            records[name] = values
            self.records[rectype] = records
            # value was added
            added = True
        # else value already in record, don't add it again
        # hack to set the ns value
        if added and rectype==ZONE_NS and len(values)==1:
            # generate ns
            if value[-1] != u".":
                ns = value+u"."+self.origin
            else:
                ns = value
            self.ns = ns
        return added
    
    # makes a record "present"
    def record_present (self, key, rectype, value):
        self.set_present()
        added = self.add_record(key, rectype, value)
        if added:
            self.changed = True

    def remove_record (self, name, rectype, value):
        if name == self.origin:
            name = u"@"
        if rectype not in self.records.keys():
            return False
        records = self.records[rectype]
        if name not in records.keys():
            return False
        values = records[name]
        if value not in values:
            return False
        values.remove(value)
        records[name] = values
        self.records[rectype] = records
        return True

    # makes a record "absent"
    def record_absent (self, key, rectype, value):
        removed = self.remove_record (key, rectype, value)
        if removed:
            self.changed = True

    #--------------------------------------------------------------------------
    # parsing helper functions
    #

    def clean_lines(self, lines):
        tl = []
        for l in lines:
            if l[-1] == '\n':
                l = l[0:-1]
            tl.append (l.strip())
        return tl

    def clean_spaces(self, line):
        s = u""
        spc = False
        for c in line:
            if c == u" ":
                if not spc:
                    s+=c
                    spc=True
            else:
                spc=False
                s+=c
        return s

    def expect_string(self, s, pos, string):
        t = s[pos]
        if t!=string:
            self.module.fail_json(msg=u"expected \'"+unicode(string)+u"\' at pos "+unicode(pos+1)+u" in "+unicode(s))

    def expect_integer(self, s, pos, variable):
        t = s[pos]
        try:
            v = int(t)
        except ValueError as e:
            self.module.fail_json(msg=u"expected integer value for "+unicode(variable)+u" at pos "+unicode(pos+1)+u" in "+unicode(s))
        return v

    #--------------------------------------------------------------------------
    # zone file parsing functions
    #

    def parse_zone_line(self, line):
        s = self.clean_spaces(line).split(u" ")
        
        t = s[0]

        # analyze line
        if t == ZONE_TTL:
            # TODO: 
            return
        if t == ZONE_ORIGIN:
            # TODO: check if self.domain is origin
            self.origin = s[1]
            return
        if t == u"@":
            name = self.origin
        else:
            name = t

        # line defined for the origin domain
        if s[1] == ZONE_IN:
            rectype = s[2]
            if rectype == ZONE_SOA:
                self.ns = s[3]
                self.email = s[4]
                self.expect_string(s, 5, u"(")
                self.serial  = self.expect_integer(s, 6, "serial")
                self.refresh = self.expect_integer(s, 7, "refresh")
                self.retry   = self.expect_integer(s, 8, "retry")
                self.expiry  = self.expect_integer(s, 9, "expiry")
                self.minimum = self.expect_integer(s, 10, "minimum")
                self.expect_string(s, 11, u")")
                self.origin = name
                return
            elif rectype == ZONE_NS:
                value = s[3]
                self.add_record(name, rectype, value) 
                return
            elif rectype == ZONE_A:
                value = s[3]
                self.add_record(name, rectype, value) 
                return
            else:
                self.module.fail_json(msg=u"expected 'SOA' in pos=3 - "+unicode(s))
        else:
            self.module.fail_json(msg=u"expected 'IN' in pos=2 - "+unicode(s))
        self.module.fail_json (msg=unicode(self.name)+u" - "+unicode(s))

    def read_zone(self):
        try:
            f = open(os.path.join(BIND_CONFIG_DIR, self.filename()), "r")
        except IOError as e:
            if e.errno == errno.ENOENT:
                return False
        lines = self.clean_lines(f.readlines())
        f.close()

        # parse content of file
        # first line should be a comment, containing ANSIBLE
        l = lines[0]
        error = False
        if l[0] == ZONE_COMMENT:
            l = l.split(u" ")
            if ZONE_ANSIBLE not in l:
                error = True
        else:
            # not a comment
            error = True
        if error:
            self.module.fail_json(msg=u"ERROR reading zone file, expected the first line to be a comment containing '"+ZONE_ANSIBLE+u"'")

        lines = lines[1:]
    
        for l in lines:
            self.parse_zone_line(l)

        return True
    
    def remove(self):
        self.module.fail_json(msg=u"remove is not implemented yet")

    #--------------------------------------------------------------------------
    # output helper functions
    #
    
    def fixed_width(self, val, size):
        s = val

        # add a space to separate this field from the next anyways
        if len(val)>=size:
            return val+u" "
        
        while len(s)<size:
            s+=u" "
        return s
   
    #--------------------------------------------------------------------------
    # global write function
    # 
    
    def write(self):
        return self.zone_list.write() and self.write_zone()

    #--------------------------------------------------------------------------
    # zone file output function
    #

    def write_zone(self):
        if not self.changed:
            return True
       
        full_path = os.path.join(BIND_CONFIG_DIR, self.filename())

        if not self.present:
            os.remove (full_path)
            return True
            
        # if changed, increment serial
        self.serial += 1

        f = open(full_path, "w")
        # handled by ansible line
        print (ZONE_ANSIBLE_MANAGED, file=f)

        # TTL line
        print (self.fixed_width(ZONE_TTL, 8)+unicode(self.ttl), file=f)
        
        # ORIGIN line
        print (self.fixed_width(ZONE_ORIGIN, 8)+self.origin, file=f)
        
        # SOA line
        dom = self.fixed_width (u"@", 24)
        email = self.email
        if email is None : 
            email = u"root." + self.origin
        print (dom+
            self.fixed_width(ZONE_IN, 8) + 
            self.fixed_width(ZONE_SOA, 8) + 
            self.ns + u" " +
            email + u" ( " +
            unicode(self.serial) + u" " +
            unicode(self.refresh) + u" " +
            unicode(self.retry) + u" " +
            unicode(self.expiry) + u" " +
            unicode(self.minimum) + u" )", 
            file=f)
        
        for rectype in self.records.keys():
            rec = self.records[rectype]
            for key in rec.keys():
                vals=rec[key]
                for v in vals:
                    k = key
                    if k == self.origin:
                        k = u"@"
                    print (self.fixed_width(k, 24)+
                        self.fixed_width(ZONE_IN, 8)+
                        self.fixed_width(rectype, 8)+
                        v, file=f)

        f.close()
        return True

class Bind9(object):

    def init_zone (self):
        z = Zone()
        z.module = self.module
        z.set_new_origin(self.zone)
        z.init()
        return z

    def manage_zone (self):
        z = self.init_zone()
        if z is None:
            self.module.fail_json(msg=u"ERROR in manage_zone "+unicode(self.zone))
        if self.present:
            z.set_present()
        else: 
            z.set_absent()
        if z.write():
            self.module.exit_json(changed=z.changed)
        # should not happen
        self.module.fail_json (msg=u'ERROR in manage_zone : adding new zone \''+unicode(self.zone)+'\'')

    def manage_record (self):
        z = self.init_zone()
        if z is None:
            self.module.fail_json(msg=u"ERROR in manage_record "+unicode(self.zone))
        if self.present:
            z.record_present (self.key, self.record, self.value)
        else:
            z.record_absent (self.key, self.record, self.value)
        if z.write():
            self.module.exit_json(changed=z.changed)
        # should not happen
        self.module.fail_json (msg=u"ERROR in manage_records : manage records done") 

    def __init__ (self):
        self.module = AnsibleModule (
            argument_spec = {
                'state':  { 'default': 'present', 'choices': [ 'present', 'absent' ] },
                'zone':   { 'type': 'str' },
                'key':    { 'type': 'str' },
                'record': { 'type': 'str' },
                'value':  { 'type': 'str' }
            }
        )

        self.state  = self.module.params['state']
        self.zone   = self.module.params['zone']
        self.key    = self.module.params['key']
        self.record = self.module.params['record']
        self.value  = self.module.params['value']

        self.present = (self.state == "present")

        if (self.zone is not None) and (self.key is None):
            self.manage_zone ()
        
        if (self.zone is not None) and (self.key is not None):
            self.manage_record ()

        self.module.fail_json (msg=u'nothing to be done')

if __name__ == "__main__" :
    Bind9()
