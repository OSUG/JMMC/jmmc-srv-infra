#
# To be sourced so we can provide completion for the main commands (starting with run)
#


_jmmc_run (){
	local playbooks=$(find $ROOT_DIR/playbooks -mindepth 1 -maxdepth 1 -type f -name "*.yml" -exec basename {} \;)
	COMPREPLY=( $( compgen -W "${playbooks}" -- ${COMP_WORDS[COMP_CWORD]} ) )
}

complete -F _jmmc_run run


