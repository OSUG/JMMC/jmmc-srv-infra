#!/bin/sh
# Author: Raphaël Jacquot
# Description: debian init script

### BEGIN INIT INFO
# Provides:             zabbix-agentd
# Required-Start:       $syslog $network
# Required-Stop:        $syslog
# Should-Start:         
# Default-Start:        2 3 4 5
# Default-Stop:     
# Short-Description:    zabbix agent daemon
# Description:          agent for the zabbix monitoring system
### END INIT INFO

set -e

PIDFILE="/tmp/zabbix_agentd.pid"
AGENTD="/usr/local/sbin/zabbix_agentd"

. /lib/lsb/init-functions

if [ -x "${AGENTD}" ] ; then 
    echo "unable to find ${AGENTD}" 
    exit 0
fi

export PATH="${PATH:+$PATH:}/usr/sbin:/sbin"

case "${1}" in
    start)
        if start-stop-daemon --start --quiet --oknodo --pidfile ${PIDFILE} --exec ${AGENTD}; then
            log_end_msg 0 || true
        else
            log_end_msg 1 || true
        fi
        ;;
    stop)
        if start-stop-daemon --stop --quiet --oknodo --pidfile ${PIDFILE} --exec ${AGENTD}; then
            log_end_msg 0 || true
        else
            log_end_msg 1 || true
        fi
        ;;
    status)
        status_of_proc -p ${PIDFILE} ${AGENTD} zabbix_agentd && exit 0 || exit $?
        ;;
    *)
        log_action_msg "Usage: /etc/init.d/zabbix-agentd {start|stop|status}" || true
        exit 1
esac

exit 0

