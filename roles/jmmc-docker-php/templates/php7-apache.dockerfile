# Author: Raphaël Jacquot
# Description: This dockerfile starts with apache/php7 and adds the xslt modules
FROM php:7.2.17-apache

RUN  apt-get update && apt-get install -y \
     libxslt1.1 libxslt1-dev \
     libgd3 libgd-dev \
  && docker-php-ext-install xsl gd

