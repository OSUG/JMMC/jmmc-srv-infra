# Author: Guillaume Mella
# Description: This dockerfile modify httpd default conf
FROM httpd:2.4.41
RUN sed -i -e 's/AllowOverride None/AllowOverride All/' /usr/local/apache2/conf/httpd.conf
